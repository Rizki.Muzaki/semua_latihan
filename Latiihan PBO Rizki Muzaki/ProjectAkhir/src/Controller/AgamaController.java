package Controller;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Model.Agama;

public class AgamaController {
    
    Connection conn = null;
    PreparedStatement stmt;
    Agama agama = new Agama();
    DefaultTableModel tb;
    ResultSet rs;
    
    public void tambahAgama(String nama){
        String sql = "INSERT INTO agama SET nama_agama = ?";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, nama);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Data Berhasil Ditambah");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Data Gagal Ditambah");
        }
    }
    
    public DefaultTableModel viewAgama(){
        Object header[] = {"ID", "Agama",};
        tb = new DefaultTableModel(null, header);
        String sql = "SELECT * FROM agama ";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while(rs.next()){
                String kolom1 = rs.getString(1);
                String kolom2 = rs.getString(2);
                String kolom[] = {kolom1,kolom2};
                tb.addRow(kolom);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Koneksi Gagal");
        }
        return tb;
    }
    
     public void updateAgama(int id, String nama){
        String sql = "UPDATE agama SET nama_agama = ? WHERE id = ?";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setInt(2, id);
            stmt.setString(1, nama);
            int hasil = stmt.executeUpdate();
            if(hasil > 0){
                JOptionPane.showMessageDialog(null, "Data Agama "
                        + "Berhasil Diperbaharui");
            }else{
                JOptionPane.showMessageDialog(null, "Data Agama "
                        + "Gagal Diperbaharui");
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Koneksi Gagal");
        }
    }
    
    public void deleteAgama(int id){
        String sql = "DELETE FROM agama WHERE id = ?";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            int hasil = stmt.executeUpdate();
            if(hasil > 0){
                JOptionPane.showMessageDialog(null, "Data Agama "
                        + "Berhasil Dihapus");
            }else{
                JOptionPane.showMessageDialog(null, "Data Agama "
                        + "Gagal Dihapu");
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Koneksi Gagal");
        }
    }
    
}