package Pertemuan5;

public class LoopingLogikaIF {
   public static void main(String[] args) {
        for(int i=1; i<=10; i++){
            if(i % 2 ==0){
                System.out.println("Angka "+i+" adalah Genap");
            }else{
                System.out.println("Angka "+i+" adalah Ganjil");
            }
        }
    }
}
